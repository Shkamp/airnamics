# Multi-component system

This is a system that enables components to be connected on their attachment points. 

## Instructions for use

### Selection and movement
Left-click on an object to move it around. Left-click again to deselect the object.

The red capsules on an object are its connectors. When they overlap the objects will snap together. 

If you wish to unsnap them move the mouse further away the object the selected object is attached to. 

### Rotation

When a selected object is attached to another object, it can be rotated around the attached connector's up axis by right-clicking.

## Game Object Components

This is a list of game objects and their descriptions

### ConstructionComponent
This is a component we use to construct our creations. Its GameObject can be clicked after it which it can be moved around. Once it is connected to another object's connector it can be rotated around the connector.

### Connector
This is attached to a child of a GameObject that holds a ConstructionComponent component. It checks if it is touching another connector and snaps the parent object to the connector it is touching.

### Grabber
Handles the logic of picking up and dropping objects.


## Execution Order
In order for the events that the scripts invoke to be ready the execution order is set to Connector->ConstructionComponent->Grabber.

## Future work
The current project is a simple MVP that is intended to illustrate the app's behaviour.

Future versions would need to implement:
- a system to handle how objects are parented to each other (object could have a preset priority, it could be handled through the UI, be dynamically assigned through the order of connecting objects... )
- a system to effectively move and rotate the objects in 3d space possibly implementing Unity's gizmos (there are libraries on Github, but did not work out-of-the box and fixing them seemed tobe out of this exercise's scope)
- multiple types of connectors with different compatibilities
