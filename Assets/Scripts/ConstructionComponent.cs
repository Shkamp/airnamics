using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConstructionComponent : MonoBehaviour
{
    // Start is called before the first frame update
    List<Transform> connectors;

    enum State
    {
        floating,
        dragging,
        snapped
    }

    public static UnityEvent<GameObject> onConstructionComponentCreated;
    void Start()
    {
        AddConnectors();
        if (onConstructionComponentCreated == null)
        {
            onConstructionComponentCreated = new UnityEvent<GameObject>();
        }
        onConstructionComponentCreated?.Invoke(gameObject);
    }

    private void AddConnectors()
    {
        connectors = new List<Transform>();
        foreach (Transform child in transform)
        {
            if (child.tag == "Connector")
            {
                connectors.Add(child);
            }
        }
    }

   public bool IsAttached(){
       return false;
   }

   
}
