using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Grabber : MonoBehaviour
{

    GameObject selectedObject;
    List<GameObject> constructionComponents;
    private GameObject lastSnappedConnector;
    Vector3 snapPosition;
    [SerializeField] float unSnapDistance = 5f;

    bool isSelectedSnapped;



    // Update is called once per frame

    private void Start()
    {
        Connector.onSnap?.AddListener(OnSelectedSnap);
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (selectedObject == null)
            {
                RaycastHit hit = CastRay();
                Debug.Log("Ray cast");
                if (hit.collider != null)
                {
                    Debug.Log("Hit");
                    if (hit.collider.tag != "ConstructionComponent")
                    {
                        return;
                    }
                    selectedObject = hit.collider.gameObject;
                    Cursor.visible = false;
                    ToggleColliders(selectedObject, true);
                }
            }
            else
            {
                ToggleColliders(selectedObject, false);
                MoveObject(true);

            }
        }

        if (selectedObject != null)
        {
            UnSnap();
            MoveObject(false);
            if (Input.GetMouseButtonDown(1) && isSelectedSnapped)
            {
                selectedObject.transform.RotateAround(lastSnappedConnector.transform.position, lastSnappedConnector.transform.up, 90f);
            }
        }
    }

    private void MoveObject(bool drop)
    {
        if (!isSelectedSnapped)
        {
            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.WorldToScreenPoint(selectedObject.transform.position).z);
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(position);
            selectedObject.transform.position = new Vector3(worldPosition.x, worldPosition.y, worldPosition.z);
        }

        if (!drop)
        {
            return;
        }
        selectedObject = null;
        Cursor.visible = true;
    }

    private RaycastHit CastRay()
    {
        Vector3 screenMousePosFar = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.farClipPlane
        );
        Vector3 screenMousePosNear = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.nearClipPlane
        );

        Vector3 worldMouseFar = Camera.main.ScreenToWorldPoint(screenMousePosFar);
        Vector3 worldMouseNear = Camera.main.ScreenToWorldPoint(screenMousePosNear);
        Debug.DrawRay(worldMouseNear, worldMouseFar - worldMouseNear, Color.red);

        RaycastHit hit;
        Physics.Raycast(worldMouseNear, worldMouseFar - worldMouseNear, out hit);
        return hit;

    }

    void AddComponentToList(GameObject constructionComponent)
    {
        if (constructionComponents == null)
        {
            constructionComponents = new List<GameObject>();
        }
        else
        {
            constructionComponents.Add(constructionComponent);
        }
    }


    void OnSelectedSnap(GameObject obj)
    {
        Debug.Log("snapping");
        if (obj.transform.parent.gameObject != selectedObject)
        {
            return;
        }
        lastSnappedConnector = obj;
        snapPosition = Input.mousePosition;
        isSelectedSnapped = true;
    }

    public void UnSnap()
    {
        if ((Input.mousePosition - snapPosition).magnitude >= unSnapDistance)
        {
            isSelectedSnapped = false;
        }
    }

    void ToggleColliders(GameObject obj, bool enable)
    {
        if(obj == null){
            return;
        }
        foreach (Transform child in obj.transform)
        {
            if (child.tag == "Connector")
            {
                child.gameObject.GetComponent<Connector>().IsSelected = enable;
            }
        }
    }
}
