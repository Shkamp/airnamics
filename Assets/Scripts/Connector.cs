using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Connector : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 parentOffset;

    public static UnityEvent<GameObject> onSnap;
    public static UnityEvent<GameObject> onPickup;

    public bool IsSelected { get; set; }

    void Start()
    {
        GetParentOffset();
        if (onSnap == null)
        {
            onSnap = new UnityEvent<GameObject>();
        }
    }

    public Vector3 GetParentOffset()
    {
        parentOffset = transform.parent.position - transform.position;
        return parentOffset;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!IsSelected)
        {
            return;
        }
        if (other.gameObject.tag == "Connector" && Vector3.Dot(other.transform.forward, transform.up) == 0)
        {
            transform.position = other.transform.position;
            transform.parent.position = other.transform.position + parentOffset;
            onSnap?.Invoke(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Connector" && Vector3.Dot(other.transform.forward, transform.up) == 0)
        {
            transform.position = transform.parent.position - parentOffset;
        }
    }

}
